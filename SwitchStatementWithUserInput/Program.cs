﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SwitchStatementWithUserInput
{
    class Program
    {
        static void Main(string[] args)
        {
            ///variable to hold the value of user, the value of kilometers/mile and miles/kilometer
            ///, and the value that user wants to calculate
            var user = "0";
            double milesperkilometer = 0.621371;
            double kilometerspermile = 1.60934;
            int value = 0;
            int number = 0;
            
            ///personalized message to greet the user and describe the program
            Console.WriteLine("Hi there what's your name?");
            user = Console.ReadLine();
            Console.WriteLine($"Thanks {user}, it's nice to talk with you");
            Console.WriteLine("This program converts kilometers to miles and vice versa");
            
            ///Prompt the user as to what they would like to convert
            Console.WriteLine($"If you would like to convert to kilometers then type '1'");
            Console.WriteLine($"If you would like to convert to miles then type '2'");
            value = int.Parse(Console.ReadLine());
            
            ///Prompt the user to choose a numerical value to convert
            if (value == 1)
            {
                Console.WriteLine($"Great, thanks {user}. So, how many miles would you like to convert into kilometers?");
            }
            else if (value == 2)
            {
                Console.WriteLine($"Great, thanks {user}. So, how many kilometers would you like to convert into miles?");
            }
            else
            {
                Console.WriteLine("That isn't an option");
            }
            number = int.Parse(Console.ReadLine());

            ///
            if (value == 1)
            {
                switch (number)
                {
                    case 42:
                        Console.WriteLine("More importantly this is the meaning of life");
                        break;

                    case 13:
                        Console.WriteLine("You're gonna die, that's so unlucky");
                        break;

                    default:
                        Console.WriteLine($"Great you'll find that {number} equates to {kilometerspermile * number}");
                        break;
                }
            }
            else if (value == 2)
            {
                switch (number)
                {
                    case 42:
                        Console.WriteLine("More importantly this is the meaning of life");
                        break;

                    case 13:
                        Console.WriteLine("You're gonna die, that's so unlucky");
                        break;

                    default:
                        Console.WriteLine($"Great you'll find that {number} equates to {milesperkilometer * number}");
                        break;
                }
            }
        }
    }
}
